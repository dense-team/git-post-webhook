<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 8.12.2017
 * Time: 10:06
 */

use Dense\Config\Config;

class ConfigTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getConfig()
    {
        return [
            'build' => [
                'dir' => '/path/to/build/directory',
            ],
            'credentials' => [
                'username' => 'USERNAME',
                'password' => 'PASSWORD',
            ],
            'projects' => [
                'PROJECT_KEY:REPOSITORY_NAME' => [ // project:repository
                    'url' => 'https://IP:PORT/url/to/repo.git',
                    'commands' => [
                        'included' => [
                            'php_packages',
                            'js_packages',
                            'assets',
                        ],
                        'custom' => [],
                    ],
                    'branches' => [
                        'master' => [ // branch
                            'paths' => [
                                '/path/to/prod/application',
                            ],
                        ],
                        'test' => [ // branch
                            'paths' => [
                                '/path/to/test/application1',
                                '/path/to/test/application2',
                            ],
                        ],
                    ],
                ],
            ],
        ];
    }

    protected function mockConfig()
    {
        $configFile = $this->getConfig();
        $config = new Config($configFile);

        return $config;
    }

    public function testProjectPaths()
    {
        $config = $this->mockConfig();

        $repositoryId = 'PROJECT_KEY:REPOSITORY_NAME';
        $branchId = 'master';
        $actual = $config->getPaths($repositoryId, $branchId);

        $expected = [
            '/path/to/prod/application',
        ];
        $this->assertEquals($actual, $expected);
    }

    public function testProjectFullUrl()
    {
        $config = $this->mockConfig();

        $repositoryId = 'PROJECT_KEY:REPOSITORY_NAME';
        $actual = $config->getFullProjectUrl($repositoryId);

        $expected = 'https://USERNAME:PASSWORD@IP:PORT/url/to/repo.git';
        $this->assertEquals($actual, $expected);
    }

    public function testBuildPath()
    {
        $config = $this->mockConfig();

        $commit = '123456789123456789123456789123456789123456789';
        $actual = $config->getFullBuildPath($commit);

        $expected = '/path/to/build/directory/' . $commit;
        $this->assertEquals($actual, $expected);
    }

    public function testIncludedCommands()
    {
        $config = $this->mockConfig();

        $repositoryId = 'PROJECT_KEY:REPOSITORY_NAME';
        $actual = $config->getIncludedCommands($repositoryId);

        $expected = [
            'php_packages',
            'js_packages',
            'assets',
        ];

        $this->assertEquals($actual, $expected);
    }

    public function testCustomCommands()
    {
        $config = $this->mockConfig();

        $repositoryId = 'PROJECT_KEY:REPOSITORY_NAME';
        $actual = $config->getCustomCommands($repositoryId);

        $expected = [];

        $this->assertEquals($actual, $expected);
    }
}
