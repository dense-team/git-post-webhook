<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.11.2016
 * Time: 20:59
 */

use Dense\Parser\Bitbucket\Parser;

class BitbucketbPayloadTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getPayload()
    {
        $jsonPath = __DIR__ . '/stubs/bitbucket/payload.json';
        return json_decode(file_get_contents($jsonPath));
    }

    protected function mockParser()
    {
        $payload = $this->getPayload();
        $parser = new Parser($payload);

        return $parser;
    }

    public function testPayloadCommit()
    {
        $parser = $this->mockParser();
        $actual = $parser->getCommit();

        $expected = '709d658dc5b6d6afcd46049c2f332ee3f515a67d';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadBranchName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getBranchName();

        $expected = 'name-of-branch';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadRepositoryName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getRepositoryName();

        $expected = 'repo_name';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadProjectName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getProjectName();

        $expected = 'Untitled project';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadProjectKey()
    {
        $parser = $this->mockParser();
        $actual = $parser->getProjectKey();

        $expected = 'proj';
        $this->assertEquals($actual, $expected);
    }

    public function testIdentifier()
    {
        $parser = $this->mockParser();
        $actual = $parser->getIdentifier();

        $expected = 'proj:repo_name';
        $this->assertEquals($actual, $expected);
    }
}
