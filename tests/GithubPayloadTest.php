<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.11.2016
 * Time: 20:03
 */

use Dense\Parser\Github\Parser;

class GithubPayloadTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getPayload()
    {
        $jsonPath = __DIR__ . '/stubs/github/payload.json';
        return json_decode(file_get_contents($jsonPath));
    }

    protected function mockParser()
    {
        $payload = $this->getPayload();
        $parser = new Parser($payload);

        return $parser;
    }

    public function testPayloadCommit()
    {
        $parser = $this->mockParser();
        $actual = $parser->getCommit();

        $expected = '0d1a26e67d8f5eaf1f6ba5c57fc3c7d91ac0fd1c';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadBranchName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getBranchName();

        $expected = 'changes';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadRepositoryName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getRepositoryName();

        $expected = 'public-repo';
        $this->assertEquals($actual, $expected);
    }

    public function testIdentifier()
    {
        $parser = $this->mockParser();
        $actual = $parser->getIdentifier();

        $expected = 'public-repo';
        $this->assertEquals($actual, $expected);
    }
}
