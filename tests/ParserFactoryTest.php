<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.11.2016
 * Time: 12:30
 */

use Dense\Parser\Factory;

class ParserFactoryTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getPayload($payloadType)
    {
        switch ($payloadType) {

            case Factory::TYPE_GITHUB:
                $jsonPath = __DIR__ . '/stubs/github/payload.json';

                break;

            case Factory::TYPE_BITBUCKET:
                $jsonPath = __DIR__ . '/stubs/bitbucket/payload.json';

                break;

            case Factory::TYPE_POSTSERVICE:
                $jsonPath = __DIR__ . '/stubs/bitbucket/plugins/post-service/payload.json';

                break;

            default:
                throw new \Exception('Invalid payload type');
                break;
        }

        return json_decode(file_get_contents($jsonPath));
    }

    public function testGithubPayloadRecognition()
    {
        $payload = $this->getPayload(Factory::TYPE_GITHUB);

        $actual = Factory::recognizePayloadType($payload);

        $expected = Factory::TYPE_GITHUB;
        $this->assertEquals($actual, $expected);
    }

    public function testBitbucketPayloadRecognition()
    {
        $payload = $this->getPayload(Factory::TYPE_BITBUCKET);

        $actual = Factory::recognizePayloadType($payload);

        $expected = Factory::TYPE_BITBUCKET;
        $this->assertEquals($actual, $expected);
    }

    public function testPostServicePayloadRecognition()
    {
        $payload = $this->getPayload(Factory::TYPE_POSTSERVICE);

        $actual = Factory::recognizePayloadType($payload);

        $expected = Factory::TYPE_POSTSERVICE;
        $this->assertEquals($actual, $expected);
    }
}
