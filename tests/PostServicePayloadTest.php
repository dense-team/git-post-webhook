<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 17.11.2016
 * Time: 17:03
 */

use Dense\Parser\Bitbucket\Plugins\PostService\Parser;

class PostServicePayloadTest extends PHPUnit_Framework_TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    protected function getPayload()
    {
        $jsonPath = __DIR__ . '/stubs/bitbucket/plugins/post-service/payload.json';
        return json_decode(file_get_contents($jsonPath));
    }

    protected function mockParser()
    {
        $payload = $this->getPayload();
        $parser = new Parser($payload);

        return $parser;
    }

    public function testPayloadCommit()
    {
        $parser = $this->mockParser();
        $actual = $parser->getCommit();

        $expected = 'f259e9032cdeb1e28d073e8a79a1fd6f9587f233';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadBranchName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getBranchName();

        $expected = 'master';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadRepositoryName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getRepositoryName();

        $expected = 'iridium-parent';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadProjectName()
    {
        $parser = $this->mockParser();
        $actual = $parser->getProjectName();

        $expected = 'Iridium';
        $this->assertEquals($actual, $expected);
    }

    public function testPayloadProjectKey()
    {
        $parser = $this->mockParser();
        $actual = $parser->getProjectKey();

        $expected = 'IR';
        $this->assertEquals($actual, $expected);
    }

    public function testIdentifier()
    {
        $parser = $this->mockParser();
        $actual = $parser->getIdentifier();

        $expected = 'IR:iridium-parent';
        $this->assertEquals($actual, $expected);
    }
}
