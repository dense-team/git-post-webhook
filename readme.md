# GIT post webhook by Dense  
Simple implementation of push event webhook.
Webhook calls 'git clone' and few optional commands like 'composer install', 'npm install' and then synchronizes files with 'rsync' command.
Supported GIT platforms are Bitbucket and Github.
Older versions of Bitbucket use plugin named Post service webhook, which is supported as well.   

## Warning  
Tested only on Linux machines.  

## Instalation  
Run following composer command.  
  
```shell
composer create-project dense/git-post-webhook DIRNAME
```
  
Set up permissions for logs directory.

```shell
chmod 755 logs
```
  
Set up your DNS or use IP address to be able to set webhook on Bitbucket or Github.  
## Configuration

Set up your project paths in config/projects.php file.  
## Versioning

Version 3 is current version. Versions 1 and 2 were based on git pull scenario and are no longer supported.  

## Notes about deployment process
Application requires docker image to run properly. Docker container takes care of additional settings. For example composer runs under any user however npm runs only under sudo.
Also in order to be able to run production optimizations, environment variable NODE_ENV has to be set to production value.   
  
Deployment process follows these steps:

1. Version controll system (Github, Bitbucket) contacts application via url and sends payload data.  
2. Application analyzes payload and sets necessary variables.  
3. Application clears all previous unfinished or failed deployment builds.
4. Application runs git clone.  
5. Application sets working directory to build directory.
6. Application runs optional commands like composer install, npm install, gulp or any custom commands defined in config fie.  
7. Application runs rsync to copy all data to destination folder.  
8. Application sets correct permissions to destination folder.  
