<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 16.11.2016
 * Time: 17:22
 */

use \Monolog\Logger;
use \Monolog\Formatter\LineFormatter;
use \Monolog\Handler\StreamHandler;

use \Dense\Payload;
use \Dense\Parser\Factory as ParserFactory;
use \Dense\Config\Config;

use \Dense\Command\Chain as CommandChain;
use \Dense\Command\General as GeneralCommand;
use \Dense\Command\Git as GitCommand;
use \Dense\Command\Composer as ComposerCommand;
use \Dense\Command\Npm as NpmCommand;
use \Dense\Command\Gulp as GulpCommand;
use \Dense\Command\Rsync as RsyncCommand;

// application root directory
define('ROOT', __DIR__ . '/../');

// logger setting
define('LOGGER', 'debug'); // simple or debug

// composer autoloading
require ROOT . '/vendor/autoload.php';

// create the logger
$formatter = new LineFormatter(null, null, false, true);

$logger = new Logger('logger');
$logger
    ->pushHandler((new StreamHandler(ROOT . '/logs/debug.log', Logger::DEBUG, false))
        ->setFormatter($formatter)
    )
    ->pushHandler((new StreamHandler(ROOT . '/logs/success.log', Logger::INFO, false))
        ->setFormatter($formatter)
    )
    ->pushHandler((new StreamHandler(ROOT . '/logs/error.log', Logger::ERROR, false))
        ->setFormatter($formatter)
    );

// projects configuration file
$configFile = require ROOT . '/config/projects.php';

try {
    // read the payload from GIT server
    $payload = Payload::obtainPayload();

    // parse the payload
    $parser = ParserFactory::create($payload);

    $repositoryId = $parser->getIdentifier();
    $branchId = $parser->getBranchName();
    //$commit = $parser->getCommit();

    // parse project paths from configuration file based on payload
    $config = new Config($configFile);
    $projectUrl = $config->getFullProjectUrl($repositoryId);
    $buildPath = $config->getFullBuildPath(md5($repositoryId));
    $projectPaths = $config->getPaths($repositoryId, $branchId);

    // included commands configuration
    $includedCommands = $config->getIncludedCommands($repositoryId);
    $customCommands = $config->getCustomCommands($repositoryId);

    // result
    $return = 0;
    $output = [];

    /**
     * command chain
     */

    if (LOGGER === 'debug') {
        $logger->addDebug(sprintf('Running php as %s.', get_current_user()));
    }

    $commandsChain = new CommandChain();

    // clear files before cloning
    ($rmCommand = new GeneralCommand('rm', $buildPath))
        ->addParam('-rf');

    $commandsChain->add($rmCommand);

    /*
    // make empty directory before cloning
    ($mkdirCommand = new GeneralCommand('mkdir', $buildPath))
        ->addParam('-p');

    $commandsChain->add($mkdirCommand);

    ($changeDirCommand = new GeneralCommand('cd', $buildPath))
        ->execute($output, $return);

    if (LOGGER === 'debug') {
        $logger->addDebug(sprintf('Executed command %s with return %s.', $changeDirCommand->toString(), $return));
    }
    */

    // clone project to build directory
    ($gitCommand = new GitCommand($buildPath))
        ->addParam('clone ' . $projectUrl)
        ->addParam('--single-branch')
        ->addParam('--depth 1')
        ->addParam('--branch ' . $branchId);
        //->execute($output, $return);

    $commandsChain->add($gitCommand);

    ($changeDirCommand = new GeneralCommand('cd', $buildPath));

    $commandsChain->add($changeDirCommand);

    if ($includedCommands || $customCommands) {

        if ($includedCommands) {
            if (in_array('php_packages', $includedCommands)) {
                ($composerCommand = new ComposerCommand())
                    ->addParam('install');

                $commandsChain->add($composerCommand);
            }
            if (in_array('js_packages', $includedCommands)) {
                ($npmCommand = new NpmCommand())
                    ->addParam('install');
                    //->addParam('--unsafe-perm')
                    //->addParam('--scripts-prepend-node-path');

                $commandsChain->add($npmCommand);
            }
            if (in_array('assets', $includedCommands)) {
                ($gulpCommand = new GulpCommand());

                $commandsChain->add($gulpCommand);
            }
        }

        if ($customCommands) {
            foreach ($customCommands as $customCommand) {
                $commandsChain->add($customCommand);
            }
        }
    }

    foreach ($projectPaths as $projectPath) {
        if (!file_exists($projectPath)) {
            $logger->addError(sprintf('Project path %s does not exists.', $projectPath));

            continue;
        }

        ($rsynCommand = new RsyncCommand($buildPath, $projectPath))
            ->addParam('-au');

        $commandsChain->add($rsynCommand);

        ($chownCommand = new GeneralCommand('chown', $projectPath))
            ->addParam('-R')
            ->addParam('www-data:www-data');

        $commandsChain->add($chownCommand);
    }

    $commandsChain->execute($output, $return);

    if (LOGGER === 'debug') {
        $logger->addDebug(sprintf('Executed command %s with return %s.', $commandsChain->toString(), $return));
    }

    if ($return === 0) {
        $projectId = $parser->getIdentifier();
        $projectLocations = implode(',', $projectPaths);

        $logger->addInfo(sprintf('Project %s deployed to paths %s.', $projectId, $projectLocations));
    } else {
        $logger->addError(implode("\n", $output));
    }
} catch (\Exception $e) {
    $logger->addError($e->getMessage());
}
