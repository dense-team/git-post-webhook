<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:17
 */

namespace Dense\Command;

class Rsync extends Command
{
    use SourceApply;
    use DestinationApply;

    /**
     * Cd constructor.
     * @param string $source
     * @param string $destination
     * @throws \Exception
     */
    public function __construct($source, $destination)
    {
        $this->setSource($source);
        $this->setDestination($destination);
    }

    /**
     * @return string
     */
    protected function getExecutable()
    {
        return 'rsync';
    }

    /**
     * @return string
     */
    protected function buildParams()
    {
        $paramsOptions = $this->getParams();

        $paramsOptions[] = $this->getSourceForOutput(true);
        $paramsOptions[] = $this->getDestinationForOutput(true);

        return trim(implode(' ', $paramsOptions));
    }
}
