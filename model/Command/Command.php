<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 29.9.2017
 * Time: 15:49
 */

namespace Dense\Command;

abstract class Command
{
    /**
     * @var string
     */
    protected $command;

    /**
     * @var array
     */
    protected $params = [];

    /**
     * @return string
     */
    abstract protected function getExecutable();

    /**
     * @return array
     */
    protected function getDefaultParams()
    {
        return [];
    }

    /**
     * @return bool
     */
    protected function requireSudo()
    {
        return false;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        $defaultParams = $this->getDefaultParams();

        return $this->params + $defaultParams;
    }

    /**
     * @param string $value
     * @return $this
     */
    public function addParam($value)
    {
        $this->params[] = $value;

        return $this;
    }

    /**
     * @return string
     */
    protected function buildParams()
    {
        $paramsOptions = $this->getParams();

        return trim(implode(' ', $paramsOptions));
    }

    /**
     * @return string
     */
    protected function buildExecutable()
    {
        $sudo = null;
        if ($this->requireSudo()) {
            $sudo = 'sudo -u www-data';
        }

        return trim($sudo . ' ' . $this->getExecutable());
    }

    /**
     * @return string
     */
    protected function buildCommand()
    {
        return trim($this->buildExecutable() . ' ' . $this->buildParams());
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->buildCommand();
    }

    /**
     * @param array $output
     * @param int $return
     */
    public function execute(array &$output = [], &$return = null)
    {
        $command = $this->buildCommand();

        $currentOutput = [];
        $currentReturn = 0;

        exec($command, $currentOutput, $currentReturn);
        //shell_exec($command);

        // merge all output messages
        $output = array_merge($output, $currentOutput);

        // combine all return values
        $return = $return | $currentReturn;
    }
}