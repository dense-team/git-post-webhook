<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:40
 */

namespace Dense\Command;

trait SourceApply
{
    /**
     * @var string
     */
    protected $source;

    /**
     * @param string $source
     * @return $this
     */
    protected function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * @param bool $trailingSlash
     * @return string
     */
    protected function getSourceForOutput($trailingSlash = false)
    {
        /*
        if (!file_exists($this->source)) {
            throw new \Exception(sprintf('Incorrect path to source %s.', $this->source));
        }
        */

        $source = $this->source;
        if ($trailingSlash === true) {
            $source .= '/';
        }

        return escapeshellarg($source);
    }
}