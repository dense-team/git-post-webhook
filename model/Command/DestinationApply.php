<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:52
 */

namespace Dense\Command;

trait DestinationApply
{
    /**
     * @var string
     */
    protected $destination;

    /**
     * @param string $destination
     * @return $this
     */
    protected function setDestination($destination)
    {
        $this->destination = $destination;

        return $this;
    }


    /**
     * @param bool $trailingSlash
     * @return string
     */
    protected function getDestinationForOutput($trailingSlash = false)
    {
        $destination = $this->destination;
        if ($trailingSlash === true) {
            $destination .= '/';
        }

        return escapeshellarg($destination);
    }
}