<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:15
 */

namespace Dense\Command;

class Composer extends Command
{
    /**
     * @return string
     */
    protected function getExecutable()
    {
        return 'composer';
    }
}
