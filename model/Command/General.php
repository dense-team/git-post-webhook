<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 7.2.2018
 * Time: 15:32
 */

namespace Dense\Command;

class General extends Command
{
    use DestinationApply;

    /**
     * @var string
     */
    protected $executable;

    /**
     * Cd constructor.
     * @param string $destination
     * @throws \Exception
     */
    public function __construct($executable, $destination)
    {
        $this->executable = $executable;

        $this->setDestination($destination);
    }

    /**
     * @return string
     */
    protected function getExecutable()
    {
        return $this->executable;
    }

    /**
     * @return string
     */
    protected function buildParams()
    {
        $paramsOptions = $this->getParams();

        $paramsOptions[] = $this->getDestinationForOutput();

        return trim(implode(' ', $paramsOptions));
    }
}
