<?php
/**
 * Created by PhpStorm.
 * User: marko
 * Date: 20.2.2018
 * Time: 21:52
 */

namespace Dense\Command;

class Chain
{
    /**
     * @var array
     */
    protected $commands = [];

    /**
     * @param mixed $command
     * @return $this
     */
    public function add($command)
    {
        if ($command instanceof \Dense\Command\Command) {
            $command = $command->toString();
        }

        $this->commands[] = $command;

        return $this;
    }

    /**
     * @return string
     */
    protected function buildChain()
    {
        /*
        $commandInstructions = array_map(function ($item) {
            return $item->toString();
        }, $this->commands);
        */

        return trim(implode(' && ', $this->commands));
    }

    /**
     * @return string
     */
    public function toString()
    {
        return $this->buildChain();
    }

    /**
     * @param array $output
     * @param int $return
     */
    public function execute(array &$output = [], &$return = null)
    {
        $currentOutput = [];
        $currentReturn = 0;

        $multiCommand = $this->buildChain();

        exec($multiCommand, $currentOutput, $currentReturn);

        // merge all output messages
        $output = array_merge($output, $currentOutput);

        // combine all return values
        $return = $return | $currentReturn;
    }
}