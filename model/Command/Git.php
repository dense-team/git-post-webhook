<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:14
 */

namespace Dense\Command;

class Git extends Command
{
    use DestinationApply;

    /**
     * Cd constructor.
     * @param string $destination
     */
    public function __construct($destination = null)
    {
        $this->setDestination($destination);
    }

    /**
     * @return string
     */
    protected function getExecutable()
    {
        return 'git';
    }

    /**
     * @return string
     */
    protected function buildParams()
    {
        $paramsOptions = $this->getParams();

        if ($this->destination) {
            $paramsOptions[] = $this->getDestinationForOutput();
        }

        return trim(implode(' ', $paramsOptions));
    }
}
