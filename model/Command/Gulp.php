<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:18
 */

namespace Dense\Command;

class Gulp extends Command
{
    /**
     * @return bool
     */
    protected function requireSudo()
    {
        return true;
    }

    /**
     * @return string
     */
    protected function getExecutable()
    {
        return 'gulp';
    }
}
