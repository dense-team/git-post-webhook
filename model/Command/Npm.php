<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 12.1.2018
 * Time: 11:16
 */

namespace Dense\Command;

class Npm extends Command
{
    /**
     * @return bool
     */
    protected function requireSudo()
    {
        return true;
    }

    /**
     * @return string
     */
    protected function getExecutable()
    {
        return 'npm';
    }
}
