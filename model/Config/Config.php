<?php
/**
 * Created by PhpStorm.
 * User: mjasan
 * Date: 8.12.2017
 * Time: 10:12
 */

namespace Dense\Config;

class Config
{
    /**
     * @var array
     */
    protected $config = [];

    /**
     * Config constructor.
     * @param array $config
     */
    public function __construct(array $config)
    {
        $this->config = $config;
    }

    /**
     * @param string $repoId
     * @return array
     * @throws \Exception
     */
    protected function getRepositoryCfg($repoId)
    {
        if (!isset($this->config['projects'][$repoId])) {
            throw new \Exception(sprintf('Repository %s is not defined in config.', $repoId));
        }
        return $this->config['projects'][$repoId];
    }

    /**
     * @param string $repoId
     * @param string $branchId
     * @return array
     * @throws \Exception
     */
    protected function getBranchCfg($repoId, $branchId)
    {
        $repositoryCfg = $this->getRepositoryCfg($repoId);

        if (!isset($repositoryCfg['branches'][$branchId])) {
            throw new \Exception(sprintf('Branch %s of repository %s is not defined in config.', $branchId, $repoId));
        }
        return $repositoryCfg['branches'][$branchId];
    }

    /**
     * @param string $repoId
     * @return string
     * @throws \Exception
     */
    protected function getProjectUrl($repoId)
    {
        $repositoryCfg = $this->getRepositoryCfg($repoId);

        if (!isset($repositoryCfg['url'])) {
            throw new \Exception(sprintf('Project url of repository %s is not defined in config.', $repoId));
        }
        return $repositoryCfg['url'];
    }

    /**
     * @param string $repoId
     * @return array
     * @throws \Exception
     */
    public function getIncludedCommands($repoId)
    {
        $repositoryCfg = $this->getRepositoryCfg($repoId);

        $commands = [];
        if (isset($repositoryCfg['commands']['included'])) {
            $commands = $repositoryCfg['commands']['included'];
        }

        return $commands;
    }

    /**
     * @param string $repoId
     * @return array
     * @throws \Exception
     */
    public function getCustomCommands($repoId)
    {
        $repositoryCfg = $this->getRepositoryCfg($repoId);

        $commands = [];
        if (isset($repositoryCfg['commands']['custom'])) {
            $commands = $repositoryCfg['commands']['custom'];
        }

        return $commands;
    }

    /**
     * @param string $repoId
     * @return string
     * @throws \Exception
     */
    protected function getProjectProtocol($repoId)
    {
        $projectUrl = $this->getProjectUrl($repoId);

        $projectProtocol = strtolower(substr($projectUrl, 0, strpos($projectUrl, '://')));

        switch($projectProtocol) {
            case 'http':
            case 'https':
            case 'ssh':
            case 'git':
                return $projectProtocol;
                break;

            default:
                throw new \Exception(sprintf('Undefined url protocol of repository %s.', $repoId));
                break;
        }
    }

    /**
     * @param string $repoId
     * @return string
     * @throws \Exception
     */
    protected function getProjectUri($repoId)
    {
        $projectUrl = $this->getProjectUrl($repoId);

        $projectUri = substr($projectUrl, strpos($projectUrl, '://') + 3);

        return $projectUri;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getCredentials()
    {
        if (!isset($this->config['credentials'])) {
            throw new \Exception(sprintf('Credentials are not defined in config.'));
        }
        return $this->config['credentials'];
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function getBuildDirectory()
    {
        if (!isset($this->config['build'])) {
            throw new \Exception(sprintf('Build directory is not defined in config.'));
        }
        return $this->config['build']['dir'];
    }

    /**
     * @param string $repoId
     * @return string
     * @throws \Exception
     */
    public function getFullProjectUrl($repoId)
    {
        $credentials = array_values($this->getCredentials());
        list($username, $password) = $credentials;

        $protocol = $this->getProjectProtocol($repoId);
        $uri = $this->getProjectUri($repoId);

        $url = $protocol . '://' . $username . ':' . $password . '@' . $uri;

        return $url;
    }

    /**
     * @param string $dir
     * @return string
     * @throws \Exception
     */
    public function getFullBuildPath($dir)
    {
        $buildDir = $this->getBuildDirectory();

        //$dir = str_replace(':', '-', $dir);

        $buildPath = implode('/', [
            $buildDir,
            $dir,
        ]);

        return $buildPath;
    }

    /**
     * @param string $repoId
     * @param string $branchId
     * @return array
     * @throws \Exception
     */
    public function getPaths($repoId, $branchId)
    {
        $branchCfg = $this->getBranchCfg($repoId, $branchId);

        return $branchCfg['paths'];
    }
}