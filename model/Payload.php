<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 17.11.2016
 * Time: 10:38
 */

namespace Dense;

class Payload
{
    /**
     * @return \stdClass
     * @throws \Exception
     */
    static public function obtainPayload()
    {
        if (isset($_REQUEST['payload']) && $_REQUEST['payload']) {
            // check payload in request
            $payload = $_REQUEST['payload'];
        } else {
            // check payload in input stream
            $payload = file_get_contents('php://input');
        }

        if (!$payload) {
            throw new \Exception('No input from GIT server.');
        }

        return json_decode($payload);
    }
}
