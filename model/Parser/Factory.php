<?php

/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 18.11.2016
 * Time: 22:00
 */

namespace Dense\Parser;

use Dense\Parser\Github\Parser as GithubParser;
use Dense\Parser\Bitbucket\Parser as BitbucketParser;
use Dense\Parser\Bitbucket\Plugins\PostService\Parser as PostServiceParser;

class Factory
{
    const TYPE_GITHUB = 'GITHUB';
    const TYPE_BITBUCKET = 'BITBUCKET';
    const TYPE_POSTSERVICE = 'POSTSERVICE';

    /**
     * @param \stdClass $payload
     * @return string
     */
    static public function recognizePayloadType(\stdClass $payload)
    {
        $payloadType = null;

        $githubRoots = ['ref', 'head_commit', 'repository', 'pusher', 'sender'];

        $isGithub = 1;
        foreach ($githubRoots as $root) {
            $isGithub &= (int)property_exists($payload, $root);
        }
        if ($isGithub === 1) {
            return self::TYPE_GITHUB;
        }

        $bitbucketRoots = ['actor', 'repository', 'push'];

        $isBitbucket = 1;
        foreach ($bitbucketRoots as $root) {
            $isBitbucket &= (int)property_exists($payload, $root);
        }
        if ($isBitbucket === 1) {
            return self::TYPE_BITBUCKET;
        }

        $bitbucketPluginPostServiceRoots = ['repository', 'refChanges', 'changesets'];

        $isBitbucketPluginPostService = 1;
        foreach ($bitbucketPluginPostServiceRoots as $root) {
            $isBitbucketPluginPostService &= (int)property_exists($payload, $root);
        }
        if ($isBitbucketPluginPostService === 1) {
            return self::TYPE_POSTSERVICE;
        }
    }


    /**
     * @param \stdClass $payload
     * @return \Dense\Parser\ParserContract
     * @throws \Exception
     */
    static public function create(\stdClass $payload)
    {
        $payloadType = self::recognizePayloadType($payload);

        switch (strtoupper($payloadType)) {

            case self::TYPE_GITHUB:

                return new GithubParser($payload);

                break;

            case self::TYPE_BITBUCKET:

                return new BitbucketParser($payload);

                break;

            case self::TYPE_POSTSERVICE:

                return new PostServiceParser($payload);

                break;

            default:
                throw new \Exception('Undefined payload type.');
                break;
        }
    }
}