<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 17.11.2016
 * Time: 18:00
 */

namespace Dense\Parser\Github;

use Dense\Parser\ParserAbstract;

class Parser extends ParserAbstract
{
    /**
     * @return \stdClass
     */
    protected function getRepository()
    {
        return $this->payload->repository;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getChanges()
    {
        $changes = $this->payload->commits;

        if (empty($changes)) {
            throw new \Exception('No changes committed in payload.');
        }

        return $changes;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getCommit()
    {
        $changes = $this->getChanges();

        $change = reset($changes);

        $commit = $change->id;

        return $commit;
    }

    /**
     * @return string
     */
    public function getBranchName()
    {
        $branch = $this->payload->ref;
        if (($pos = strrpos($branch, '/')) !== false){
            $branch = substr($branch, $pos + 1);
        }

        return $branch;
    }

    /**
     * @return string
     */
    public function getRepositoryName()
    {
        $repository = $this->getRepository();

        return $repository->name;
    }

    /**
     * @return string
     */
    public function getIdentifier()
    {
        return $this->getRepositoryName();
    }
}
