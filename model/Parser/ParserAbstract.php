<?php

/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.11.2016
 * Time: 20:45
 */

namespace Dense\Parser;

abstract class ParserAbstract implements ParserContract
{
    /**
     * @var \stdClass
     */
    protected $payload;

    /**
     * ParserAbstract constructor.
     * @param \stdClass $payload
     */
    public function __construct(\stdClass $payload)
    {
        $this->payload = $payload;
    }

    /**
     * @param array $config
     * @return array
     * @throws \Exception
     */
    public function getPathsFromConfig(array $config)
    {
        $repoId = $this->getIdentifier();
        if (!isset($config[$repoId])) {
            throw new \Exception(sprintf('Repository %s is not defined in config.', $repoId));
        }
        $repositoryCfg = $config[$repoId];

        $branch = $this->getBranchName();
        if (!isset($repositoryCfg[$branch])) {
            throw new \Exception(sprintf('Branch %s is not defined in config.', $branch));
        }
        $branchCfg = $repositoryCfg[$branch];

        return $branchCfg;
    }
}
