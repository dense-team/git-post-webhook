<?php

/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 19.11.2016
 * Time: 20:50
 */

namespace Dense\Parser;

interface ParserContract
{
    /**
     * @return string
     */
    public function getIdentifier();

    /**
     * @return string
     */
    public function getBranchName();

    /**
     * @return string
     */
    public function getCommit();

    /**
     * @param array $config
     * @return array
     */
    public function getPathsFromConfig(array $config);
}