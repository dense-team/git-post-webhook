<?php
/**
 * Created by DENSE.
 * User: Marek Jasan
 * Date: 17.11.2016
 * Time: 18:00
 */

namespace Dense\Parser\Bitbucket;

use Dense\Parser\ParserAbstract;

class Parser extends ParserAbstract
{
    /**
     * @return \stdClass
     */
    protected function getRepository()
    {
        return $this->payload->repository;
    }

    /**
     * @return \stdClass
     * @throws \Exception
     */
    protected function getProject()
    {
        $repo = $this->getRepository();

        if (!$repo->project) {
            throw new \Exception('No project detected in payload.');
        }

        return $repo->project;
    }

    /**
     * @return array
     * @throws \Exception
     */
    protected function getChanges()
    {
        $changes = $this->payload->push->changes;

        if (empty($changes)) {
            throw new \Exception('No changes committed in payload.');
        }

        return $changes;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getCommit()
    {
        $changes = $this->getChanges();

        $change = reset($changes);

        $commit = $change->new->target->hash;

        return $commit;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getBranchName()
    {
        $changes = $this->getChanges();

        $change = reset($changes);

        $branch = $change->new->name;
        if (($pos = strrpos($branch, '/')) !== false){
            $branch = substr($branch, $pos + 1);
        }

        return $branch;
    }

    /**
     * @return string
     */
    public function getRepositoryName()
    {
        $repository = $this->getRepository();

        return $repository->name;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getProjectName()
    {
        $project = $this->getProject();

        return $project->project;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getProjectKey()
    {
        $project = $this->getProject();

        return $project->key;
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getIdentifier()
    {
        return $this->getProjectKey() . ':' . $this->getRepositoryName();
    }

}
